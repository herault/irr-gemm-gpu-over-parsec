/*
 * Copyright (c) 2009-2017 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 *
 * @precisions normal z -> s d c
 *
 */

#include <math.h>
#include <unistd.h>
#include <getopt.h>

#include "parsec.h"
#include "parsec/execution_stream.h"
#include "parsec/mca/pins/pins.h"
#include "parsec/profiling.h"
#include "parsec/utils/mca_param.h"

#include "irr_bs_tm.h"
#include "irr_bs_tm_init.h"
#include "gemm_summit.h"

//static unsigned long long int Rnd64seed = 100;
#define Rnd64_A  6364136223846793005ULL
#define Rnd64_C  1ULL
#define RndF_Mul 5.4210108624275222e-20f
#define RndD_Mul 5.4210108624275222e-20
#define EPSILON  0.000001L

#define imax(a, b) (( (a) > (b) )?(a):(b))
#define imin(a, b) (( (a) < (b) )?(a):(b))

static unsigned int weird_tiling(int MB, int p)
{
  return MB + (((p%2) == 0) ? +1 : -1) * (1 + 2 *(p/2));
}

static void init_tiling(unsigned int *T, unsigned long long int *seed, int MT, int MB, int M, int mca_random_tiling)
{
    int t;
    (void)seed;
    (void)mca_random_tiling;

    for (t = 0; t < MT; ++t) T[t] = MB;
    if (M%MB != 0 && MT > 0) T[MT-1] = M%MB;
    /* good old regular tiling with smaller last tile */

    if (mca_random_tiling) {
        /*
          std::vector<unsigned int> result(ntiles+1);
          result[0] = 0;
          for(long t=0; t!=ntiles-1; ++t) {
              result[t+1] = result[t] + average_tile_size + ((t%2==0)?(t+1):(-t));
          }
          result[ntiles] = range_size;
        */
        if (MT >= MB) fprintf(stderr, "Warning, MT should not be greater than MB, aborting.\n" );

        int p1, p2, b1, b2, t_root = sqrt(MT);
        b1 = sqrt(MB);
        b2 = sqrt(MB);

        fprintf(stderr, "Tiling:");
        for (p1 = 0; p1 < t_root; ++p1)
            fprintf(stderr, " %d", weird_tiling(b1, p1));
        fprintf(stderr, "\n");

        for (p1 = 0; p1 < t_root; ++p1) {
            for (p2 = 0; p2 < t_root; ++p2) {
                T[p2 + p1 * t_root] = weird_tiling(b1, p1) * weird_tiling(b2, p2);
            }
        }
    }
}

typedef struct init_irregular_tile_op_arg_s {
    int max_tile_size;
    int zero;
    int seed;
    parsec_complex64_t **storage;
} init_irregular_tile_op_arg_t;

static int init_irregular_tile_op( struct parsec_execution_stream_s *es,
                                   irr_bs_tm_t *M,
                                   int m, int n,
                                   void *args )
{
    init_irregular_tile_op_arg_t *arg = (init_irregular_tile_op_arg_t*)args;
    int i, mb = M->Mtiling[m];
    int j, nb = M->Ntiling[n];
    int jump;
    parsec_complex64_t *array;
    uint32_t idx = ((parsec_data_collection_t*)M)->data_key((parsec_data_collection_t*)M, m, n);
    
    (void)es;
    posix_memalign((void**)&array, PARSEC_ARENA_ALIGNMENT_CL1, sizeof(parsec_complex64_t)*mb*nb);
    if( arg->zero ) {
        memset(array, 0, sizeof(parsec_complex64_t)*mb*nb);        
    } else {
        jump = arg->max_tile_size * ( m * M->mt + n ) + arg->seed;
        for (j = 0; j < nb; ++j) {
            for (i = 0; i < mb; ++i) {
                array[i+j*mb] = 0.5f - jump * RndF_Mul;
                jump = Rnd64_A * jump + Rnd64_C;
#if defined(PRECISION_z) || defined(PRECISION_c)
                array[i+j*mb] += I*(.5f - jump * RndF_Mul);
                jump = Rnd64_A * jump + Rnd64_C;
#endif
            }
        }
    }
    unsigned int rank = M->super.rank_of( &M->super, m ,n );
    irr_bs_tm_set_data(M, array, m, n, M->Mtiling[m], M->Ntiling[n], 0, rank);
    arg->storage[idx] = array;

    return 0;
}

static void irregular_matrix_set_data_distribution(irr_bs_tm_t *M, parsec_complex64_t **storage_map, uint32_t *distribution)
{
    int m, n;
    for (m = 0; m < M->mt; m++) {
        for (n = 0; n < M->nt; n++) {
            uint32_t idx = ((parsec_data_collection_t*)M)->data_key((parsec_data_collection_t*)M, m, n);
            unsigned int rank = (!distribution) ? irr_bs_tm_tile_owner(M, m, n) : distribution[idx];
            /* We set the data at NULL for now, a parallel JDF running the init_irregular_tile_op on the
             * local tiles will provide data for the local part */
            irr_bs_tm_set_data(M, NULL, m, n, M->Mtiling[m], M->Ntiling[n], 0, rank);
            storage_map[idx] = NULL;
        }
    }
}

static void fini_matrix(parsec_complex64_t **Mstorage, int nb)
{
    int i;
    for (i = 0; i < nb; ++i)
        free(Mstorage[i]);
}

#if defined(PARSEC_DEBUG_PARANOID)
static void print_matrix_data(irr_bs_tm_t* A, const char *Aid, parsec_complex64_t* checkA)
{
#if defined(PRECISION_z)
#define FORMAT " %f+i%f%s"
#elif defined(PRECISION_c)
#define FORMAT " %lf+i%lf%s"
#elif defined(PRECISION_d)
#define FORMAT " %lf%s"
#else
#define FORMAT " %f%s"
#endif

#if defined(PRECISION_z) || defined(PRECISION_c)
#define cmplx_print(z) creal(z), cimag(z)
#else
#define cmplx_print(z) (z)
#endif

    /* print the matrix in scilab-friendly-ready-to-c/c format */
    int i, j;
    fprintf(stdout, "Matrix_%s = [\n", Aid);
    for (i = 0; i < A->m; i++)
        for (j = 0; j < A->n; ++j)
            fprintf(stdout, FORMAT, cmplx_print(checkA[i+A->m*j]),
                    (j!=A->n-1)?",":(i!=A->m-1)?";\n":"];\n");
}
#endif

#if defined(PARSEC_DEBUG_NOISIER)
/* prints meta deta of the matrix */
static void print_matrix_meta(irr_bs_tm_t* A)
{
    fprintf(stdout, "  Grid: %dx%d\n",A->grid.rows, A->grid.cols);
    fprintf(stdout, "  M=%d, N=%d, MT=%d, NT=%d\n", A->m, A->n, A->mt, A->nt);

    int i;
    fprintf(stdout, "  M tiling:");
    for (i = 0; i < A->mt; ++i) fprintf(stdout, " %d", A->Mtiling[i]);
    fprintf(stdout, "\n");
    fprintf(stdout, "  N tiling:");
    for (i = 0; i < A->nt; ++i) fprintf(stdout, " %d", A->Ntiling[i]);
    fprintf(stdout, "\n");

    fprintf(stdout, "  i=%d, j=%d, nb_local_tiles=%d\n", A->i, A->j, A->nb_local_tiles);
    fprintf(stdout, "  lm=%d, ln=%d, lmt=%d, lnt=%d\n", A->lm, A->ln, A->lmt, A->lnt);
}
#endif

int main(int argc, char ** argv)
{
    parsec_context_t* parsec;
    int info_solution = 0;
    unsigned long long int Aseed = 3872;
    unsigned long long int Bseed = 4674;
    unsigned long long int Tseed = 4242;
    int rank  = 0;
    int nodes = 1;
    int cores = 1;
    int gpus  = 0;
    int P = -1, Q = -1, M = -1, N = -1, K = -1, MT = -1, NT = -1, KT = -1;
    int b = -1, c = -1, d = -1, l = -1;
    int MB = -1, NB = -1, KB = -1;
    parsec_complex64_t alpha = 1., beta = 0.0;
    char **pargv;
    int pargc;
#if defined(PRECISION_z) || defined(PRECISION_c)
    alpha -= I * 0.32;
#endif

    while (1) {
        int r;
        int option_index = 0;
        int this_option_optind = optind ? optind : 1;
        static struct option long_options[] = {
            {"P",     required_argument, 0,  'P' },
            {"Q",     required_argument, 0,  'Q' },
            {"M",     required_argument, 0,  'M' },
            {"N",     required_argument, 0,  'N' },
            {"K",     required_argument, 0,  'K' },
            {"MT",    required_argument, 0,  'm' },
            {"NT",    required_argument, 0,  'n' },
            {"KT",    required_argument, 0,  'k' },
            {"b",     required_argument, 0,  'b' },
            {"c",     required_argument, 0,  'c' },
            {"d",     required_argument, 0,  'd' },
            {"l",     required_argument, 0,  'l' },
            {"gpus",  required_argument, 0,  'g' },
            {"help",  no_argument,       0,  'h' },
            {0,         0,                 0,  0 }
        };

        r = getopt_long(argc, argv, "P:Q:M:N:K:m:n:k:b:c:d:l:g:h",
                        long_options, &option_index);
        if (r == -1)
            break;

        switch (r) {
        case 'P':
            P = atoi(optarg);
            break;
        case 'Q':
            Q = atoi(optarg);
            break;
        case 'M':
            M = atoi(optarg);
            break;
        case 'N':
            N = atoi(optarg);
            break;
        case 'K':
            K = atoi(optarg);
            break;
        case 'm':
            MT = atoi(optarg);
            break;
        case 'n':
            NT = atoi(optarg);
            break;
        case 'k':
            KT = atoi(optarg);
            break;
        case 'b':
            b = atoi(optarg);
            break;
        case 'c':
            c = atoi(optarg);
            break;
        case 'd':
            d = atoi(optarg);
            break;
        case 'l':
            l = atoi(optarg);
            break;
        case 'g':
            gpus = atoi(optarg);
            break;

        case '?':
        case 'h':
            fprintf(stderr, "Usage: %s [options] -- [parsec options]\n"
                    "Where options are:\n"
                    "  [P|Q]: process grid size\n"
                    "  [M|N|K]: problem size\n"
                    "  [MT|NT|KT]: number of tiles\n"
                    "  [b|c|d|l]: gemm summit parameters\n"
                    "  --gpu|-g: number of GPUs per process\n",
                    argv[0]);
            break;

        default:
            printf("?? getopt returned character code 0%o ??\n", r);
        }
    }

    if( M == -1 )
        M = N;
    if( K == -1 )
        K = N;
    if( MT == -1 )
        MT = NT;
    if( KT == -1 )
        KT = NT;
    MB = M/MT;
    NB = N/NT;
    KB = K/KT;
    if( imin(imin(imin(MT, KT), imin(NT, M)), imin( imin(N, imin(K, b)), imin(imin(c, d), l)) ) <= 0 ) {
        fprintf(stderr, "One of M[%d], N[%d], K[%d], MT[%d], NT[%d], KT[%d], b[%d], c[%d], d[%d], l[%d] at least is not defined (or defined to 0 or -1).\n",
                M, N, K, MT, NT, KT, b, c, d, l);
        exit(1);
    }
        
#if defined(DISTRIBUTED)
    {
        int provided;
        MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &nodes);
    }
#endif

    if(rank == -1) {
      volatile int loop = 1;
      char hostname[64];
      gethostname(hostname, 64);
      fprintf(stderr, "on %s gdb -p %p\n", hostname, getpid());
      while(loop) {
        sleep(1);
      }
    }


    if(P == -1) {
      if(Q == -1) {
        Q = 1;
      }
      P = nodes/Q;
    } else {
      if(Q == -1) {
        Q = nodes/P;
      }
    }
    if( P * Q != nodes ) {
      fprintf(stderr, "*** P = %d, Q = %d, world size = %d *** Bailing out.\n", P, Q, nodes);
      exit(1);
    }

    pargc = argc - optind + 1;
    pargv = (char**)malloc((pargc+2)*sizeof(char*));
    pargv[0] = argv[0];
    for(int i = 1; i < pargc; i++) {
      pargv[i] = argv[i+optind-1];
    }
    pargv[pargc] = NULL;
    
    /* Initialize PaRSEC */
    parsec = parsec_init(cores, &pargc, &pargv);

    unsigned int *Mtiling = NULL;
    unsigned int *Ktiling = NULL;
    unsigned int *Ntiling = NULL;

    uint32_t* Adistribution = NULL;
    uint32_t* Bdistribution = NULL;
    uint32_t* Cdistribution = NULL;

    int tiledarraycase;

    parsec_mca_param_reg_int_name("gemm", "import_tiledarray", "Boolean for importing TiledArray testcase.", false, false, 0, &tiledarraycase);

    if ( 0 < tiledarraycase ) { /* Read from file */
        FILE *Aptr, *Bptr, *Cptr;
        char *ta_path;

        parsec_mca_param_reg_string_name("gemm", "ta_path",
                                         "File describing TiledArray data shape and distribution.\n",
                                         false, false,
                                         "", &ta_path);

        char Afile[256], Bfile[256], Cfile[256];
        sprintf(Afile, "%s/Adist.mat", ta_path);
        sprintf(Bfile, "%s/Bdist.mat", ta_path);
        sprintf(Cfile, "%s/Cdist.mat", ta_path);

        tiledarraycase = 0;
        if (NULL == (Aptr = fopen(Afile, "r")))
            parsec_warning("File Adist.mat not found in %s. Provide a correct path.\nFalling back to random test.\n", ta_path);
        else if (NULL == (Bptr = fopen(Bfile, "r")))
            parsec_warning("File Bdist.mat not found in %s. Provide a correct path.\nFalling back to random test.\n", ta_path);
        else if (NULL == (Cptr = fopen(Cfile, "r")))
            parsec_warning("File Cdist.mat not found in %s. Provide a correct path.\nFalling back to random test.\n", ta_path);
        else {
            tiledarraycase = 1;
            /* Read TiledArray test case */
            unsigned int amt, ant, bmt, bnt, cmt, cnt, mb, nb, kb, i, j, p;
            int k;
            fscanf(Aptr, "%u %u", &amt, &ant);
            fscanf(Bptr, "%u %u", &bmt, &bnt);
            fscanf(Cptr, "%u %u", &cmt, &cnt);
            MT = amt;
            KT = ant;
            NT = bnt;

            Adistribution = (uint32_t*)calloc(MT*KT, sizeof(uint32_t));
            Bdistribution = (uint32_t*)calloc(KT*NT, sizeof(uint32_t));
            Cdistribution = (uint32_t*)calloc(MT*NT, sizeof(uint32_t));

            Mtiling = (unsigned int*)calloc(MT, sizeof(unsigned int));
            Ntiling = (unsigned int*)calloc(NT, sizeof(unsigned int));
            Ktiling = (unsigned int*)calloc(KT, sizeof(unsigned int));

            M = N = K = 0;
            for (k = 0; k < 2*MT*KT; ++k) {
                fscanf(Aptr, "%u %u %u %u %u", &i, &j, &mb, &kb, &p);
                Mtiling[i] = mb;
                Ktiling[j] = kb;
                uint32_t idx = (i * KT) + j;
                Adistribution[idx] = p%parsec->nb_nodes;
            }

            for (k = 0; k < 2*KT*NT; ++k) {
                fscanf(Bptr, "%u %u %u %u %u", &i, &j, &kb, &nb, &p);
                if (Ktiling[i] != kb) fprintf(stdout, "Bdist tile (%u;%u) has a mismatching kb = %u, previous value K[i] was %u\n", i, j, kb, Ktiling[i]);
                Ktiling[i] = kb;
                Ntiling[j] = nb;
                uint32_t idx = (i * NT) + j;
                Bdistribution[idx] = p%parsec->nb_nodes;
            }

            for (k = 0; k < 2*MT*NT; ++k) {
                fscanf(Cptr, "%u %u %u %u %u", &i, &j, &mb, &nb, &p);
                if (Mtiling[i] != mb) fprintf(stdout, "Cdist tile (%u;%u) has a mismatching mb = %u, previous value M[i] was %u\n", i, j, mb, Mtiling[i]);
                if (Ntiling[j] != nb) fprintf(stdout, "Cdist tile (%u;%u) has a mismatching nb = %u, previous value N[j] was %u\n", i, j, nb, Ntiling[j]);
                Mtiling[i] = mb;
                Ntiling[j] = nb;
                uint32_t idx = (i * NT) + j;
                Cdistribution[idx] = p%parsec->nb_nodes;
            }

            for (k = 0; k < MT; ++k) M += Mtiling[k];
            for (k = 0; k < NT; ++k) N += Ntiling[k];
            for (k = 0; k < KT; ++k) K += Ktiling[k];

            if (0 == rank) fprintf(stdout, "M:%d, K:%d, N:%d\n", M, K, N);

            MB = M/MT;
            NB = N/NT;
            KB = K/KT;

            fclose(Aptr);
            fclose(Bptr);
            fclose(Cptr);
        }
    }

    if ( !tiledarraycase ) {
        Mtiling = (unsigned int*)malloc(MT*sizeof(unsigned int));
        Ktiling = (unsigned int*)malloc(KT*sizeof(unsigned int));
        Ntiling = (unsigned int*)malloc(NT*sizeof(unsigned int));

        KB = 1+(K-1)/KT;

        int mca_random_tiling;
        parsec_mca_param_reg_int_name("gemm", "random_tiling",
                                      "GEMM test will generate a random tiling based on MB, NB, KB",
                                      false, false,
                                      0, &mca_random_tiling);

        init_tiling(Mtiling, &Tseed, MT, MB, M, mca_random_tiling);
        init_tiling(Ntiling, &Tseed, NT, NB, N, mca_random_tiling);
        init_tiling(Ktiling, &Tseed, KT, KB, K, mca_random_tiling);
    }

    if (rank == 0) {
        int i;
        fprintf(stdout, "(MT = %d, mean({MB}) = %d) x (KT = %d, mean({KB}) = %d) x (NT = %d, mean({NB}) = %d)\n",
                MT, MB, KT, KB, NT, NB);
        for (i = 0; i < MT; ++i)
            fprintf(stdout, "%s%d%s", (i == 0)?"M tiling: ":" ", Mtiling[i], (i == MT-1)?"\n":"");
        for (i = 0; i < KT; ++i)
            fprintf(stdout, "%s%d%s", (i == 0)?"K tiling: ":" ", Ktiling[i], (i == KT-1)?"\n":"");
        for (i = 0; i < NT; ++i)
            fprintf(stdout, "%s%d%s", (i == 0)?"N tiling: ":" ", Ntiling[i], (i == NT-1)?"\n":"");
    }


    /* initializing matrix structure */
    irr_bs_tm_t ddescA;
    irr_bs_tm_init(&ddescA, matrix_ComplexDouble,
                   nodes, rank, M, K, MT, KT,
                   Mtiling, Ktiling,
                   0, 0, MT, KT, P, IRR_TM_STATIC, NULL);
    irr_bs_tm_t ddescB;
    irr_bs_tm_init(&ddescB, matrix_ComplexDouble,
                   nodes, rank, K, N, KT, NT,
                   Ktiling, Ntiling,
                   0, 0, KT, NT, P, IRR_TM_STATIC, NULL);
    irr_bs_tm_t ddescC;
    irr_bs_tm_init(&ddescC, matrix_ComplexDouble,
                   nodes, rank, M, N, MT, NT,
                   Mtiling, Ntiling,
                   0, 0, MT, NT, P, IRR_TM_STATIC, NULL);

    unsigned int max_tile = imax(ddescA.max_tile, imax(ddescB.max_tile, ddescC.max_tile));
    unsigned int max_mb = imax(ddescA.max_mb, imax(ddescB.max_mb, ddescC.max_mb));
    ddescA.max_tile = ddescB.max_tile = ddescC.max_tile = max_tile;
    ddescA.max_mb = ddescB.max_mb = ddescC.max_mb = max_mb;

    parsec_complex64_t **Astorage = (parsec_complex64_t**)calloc(MT*KT, sizeof(parsec_complex64_t*));
    parsec_complex64_t **Bstorage = (parsec_complex64_t**)calloc(KT*NT, sizeof(parsec_complex64_t*));
    parsec_complex64_t **Cstorage = (parsec_complex64_t**)calloc(MT*NT, sizeof(parsec_complex64_t*));

    /* matrix generation */
    printf("+++ Generate matrices metadata ... ");
    irregular_matrix_set_data_distribution(&ddescA, Astorage, Adistribution);
    irregular_matrix_set_data_distribution(&ddescB, Bstorage, Bdistribution);
    irregular_matrix_set_data_distribution(&ddescC, Cstorage, Cdistribution);

    printf("+++ Generate matrices local data ... ");
    init_irregular_tile_op_arg_t ddescA_init_arg = {
        .max_tile_size = max_tile,
        .zero = 0,
        .seed = Aseed,
        .storage = Astorage
    };
    parsec_irr_bs_tm_init_taskpool_t *ddescA_init_tp =
        parsec_irr_bs_tm_init_new(&ddescA, init_irregular_tile_op, &ddescA_init_arg);
    parsec_context_add_taskpool(parsec, (parsec_taskpool_t *)ddescA_init_tp);
    init_irregular_tile_op_arg_t ddescB_init_arg = {
        .max_tile_size = max_tile,
        .zero = 0,
        .seed = Bseed,
        .storage = Bstorage
    };
    parsec_irr_bs_tm_init_taskpool_t *ddescB_init_tp =
        parsec_irr_bs_tm_init_new(&ddescB, init_irregular_tile_op, &ddescB_init_arg);
    parsec_context_add_taskpool(parsec, (parsec_taskpool_t *)ddescB_init_tp);
    init_irregular_tile_op_arg_t ddescC_init_arg = {
        .max_tile_size = max_tile,
        .zero = 1,
        .seed = 0,
        .storage = Cstorage
    };
    parsec_irr_bs_tm_init_taskpool_t *ddescC_init_tp =
        parsec_irr_bs_tm_init_new(&ddescC, init_irregular_tile_op, &ddescC_init_arg);
    parsec_context_add_taskpool(parsec, (parsec_taskpool_t *)ddescC_init_tp);

    parsec_context_start(parsec);
    parsec_context_wait(parsec);

    printf("Done\n");

    free(Mtiling);
    free(Ntiling);
    free(Ktiling);

#if defined(PARSEC_DEBUG_NOISIER)
    fprintf(stdout, "Matrix A:\n");
    print_matrix_meta(&ddescA);
    fprintf(stdout, "Matrix B:\n");
    print_matrix_meta(&ddescB);
    fprintf(stdout, "Matrix C:\n");
    print_matrix_meta(&ddescC);
#endif

    double gflops = -1.0, flops = 2.0 * M * N * K;

    /* Create Parsec taskpool */
    for(int run = 0; run < 1; run++) {
        struct timeval start, end, diff;
        parsec_devices_release_memory();

        parsec_taskpool_t* PARSEC_zgemm_summit = zgemm_summit_New(alpha, (irr_bs_tm_t*)&ddescA,
                                                                  (irr_bs_tm_t*)&ddescB, beta,
                                                                  (irr_bs_tm_t*)&ddescC,
                                                                  b, c, d,
                                                                  P, Q,
                                                                  l);
        parsec_context_add_taskpool(parsec, PARSEC_zgemm_summit);

        /* lets rock! */
#if defined(DISTRIBUTED)
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        gettimeofday(&start, NULL);
        parsec_context_start(parsec);
        parsec_context_wait(parsec);
#if defined(DISTRIBUTED)
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        gettimeofday(&end, NULL);
        if( 0 == rank ) {
            double time_elapsed;
            timersub(&end, &start, &diff);
            time_elapsed = (double)diff.tv_sec + (double)diff.tv_usec/1e6;
            printf("ZGEMM_SUMMIT\tPxQ= %3d %-3d NB= %4d N= %7d : %14f gflops\n",
                   P, Q, NB, N,
                   gflops=(flops/1e9)/time_elapsed);
        }
        zgemm_summit_Destruct( PARSEC_zgemm_summit );

        parsec_devices_reset_load(parsec);
    }

    fini_matrix(Astorage, MT*KT);    free(Astorage);
    fini_matrix(Bstorage, KT*NT);    free(Bstorage);
    fini_matrix(Cstorage, MT*NT);    free(Cstorage);

    if (Adistribution) free(Adistribution);
    if (Bdistribution) free(Bdistribution);
    if (Cdistribution) free(Cdistribution);

    irr_bs_tm_destroy( (irr_bs_tm_t*)&ddescA);
    irr_bs_tm_destroy( (irr_bs_tm_t*)&ddescB);
    irr_bs_tm_destroy( (irr_bs_tm_t*)&ddescC);

    parsec_fini(&parsec);
#if defined(DISTRIBUTED)
    MPI_Finalize();
#endif

    return 0;
}
