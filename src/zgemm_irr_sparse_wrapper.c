/*
 * Copyright (c) 2020      The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * $COPYRIGHT
 *
 * @precisions normal z -> s d c
 *
 */

#include <errno.h>
#include <string.h>
#include <pthread.h>

#include "parsec/data_dist/matrix/two_dim_rectangle_cyclic.h"
#include "irr_bs_tm.h"
#include "gemm_irr_sparse.h"
#include "zgemm_irr_sparse_genB.h"
#include "parsec/mca/device/cuda/device_cuda_internal.h"

#include "parsec/utils/mca_param.h"
static two_dim_block_cyclic_t TrivDist;
static int TrivDistInitialized = 0;

static parsec_data_t *TrivDist_data_of(parsec_data_collection_t *d, ...)
{
    (void)d;
    assert(0);
    return NULL;
}

static parsec_data_t *TrivDist_data_of_key(parsec_data_collection_t *d, parsec_data_key_t key)
{
    (void)d;
    (void)key;
    assert(0);
    return NULL;
}

static parsec_key_fn_t default_hash_functions = {
    .key_equal = parsec_hash_table_generic_64bits_key_equal,
    .key_print = parsec_hash_table_generic_64bits_key_print,
    .key_hash  = parsec_hash_table_generic_64bits_key_hash
};

/**
 *******************************************************************************
 *
 *  zgemm_irr_sparse_New - Generates the taskpool that performs one of the following
 *  matrix-matrix operations for irregular sparse tiling,
 *
 *    \f[ C = \alpha [op( A )\times op( B )] + \beta C,
 *
 *  where op( X ) is one of
 *
 *    op( X ) = X  or op( X ) = X' or op( X ) = conjg( X' )
 *
 *  alpha is scalar, and A, B and C are matrices, with op( A )
 *  an m by k matrix, op( B ) a k by n matrix and C an m by n matrix.
 *
 *******************************************************************************
 *
 * @param[in] alpha
 *          alpha specifies the scalar alpha
 *
 * @param[in] A
 *          Descriptor of the distributed matrix A.
 *
 * @param[in] B
 *          Descriptor of the distributed matrix B.
 *
 * @param[in] beta
 *          beta specifies the scalar beta
 *
 * @param[out] C
 *          Descriptor of the distributed matrix C.
 *          On exit, the data described by C contain the matrix (
 *          alpha*op( A )*op( B ) )
 *
 *******************************************************************************
 *
 * @return
 *          \retval NULL if incorrect parameters are given.
 *          \retval The parsec taskpool describing the operation that can be
 *          enqueued in the runtime with parsec_enqueue(). It, then, needs to be
 *          destroy with zgemm_irr_sparse_Destruct();
 ******************************************************************************/
parsec_taskpool_t*
zgemm_irr_sparse_New( parsec_context_t *parsec,
                      parsec_complex64_t alpha, irr_bs_tm_t *A,
                      irr_bs_tm_t *B,
                      parsec_complex64_t beta,
                      irr_bs_tm_t *C,
                      int P, int Q,
                      gemm_irr_sparse_plan_t *plan,
                      int nb_gpus, int *dev_index,
                      int RL, int CL, int GL)
{
    parsec_taskpool_t* zgemm_handle = NULL;
    parsec_arena_datatype_t* adt;
    irr_bs_tm_t *Ccopy = (irr_bs_tm_t*)malloc(sizeof(irr_bs_tm_t));
    irr_bs_tm_t *Arep = (irr_bs_tm_t*)malloc(sizeof(irr_bs_tm_t));
    int *Ccopy_tiling;
    gemm_irr_sparse_comm_thread_arg_t *th_arg;

    if( TrivDistInitialized == 0 ) {
        TrivDistInitialized = 1;
        assert(A->grid.rank < P*Q);
        two_dim_block_cyclic_init(&TrivDist, matrix_ComplexDouble, matrix_Tile,
                                  A->grid.rank,
                                  1,   1,   /* Tile size */
                                  P,   Q,   /* Global matrix size (what is stored)*/
                                  0,   0,    /* Staring point in the global matrix */
                                  P,   Q,    /* Submatrix size (the one concerned by the computation */
                                  P,   Q,    /* Process grid */
                                  1,   1,    /* Process grid cyclicity */
                                  0,   0     /* Process grid offset */ );
        TrivDist.super.super.data_of = TrivDist_data_of;
        TrivDist.super.super.data_of_key = TrivDist_data_of_key;
        TrivDist.super.super.key_base = strdup("TrivDist");
        TrivDist.super.super.key_dim = strdup("");
    }
    assert(P == TrivDist.grid.rows);
    assert(Q == TrivDist.grid.cols);

    int maxmtnt = (C->mt < C->nt ? C->nt : C->mt);
    Ccopy_tiling = (int*)malloc( maxmtnt*sizeof(int) );
    for(int i = 0; i < maxmtnt; i++) Ccopy_tiling[i] = 1;
    irr_bs_tm_init(Ccopy, C->mtype, C->super.nodes, C->super.myrank,
                   C->lm, C->ln, C->lmt, C->lnt,
                   C->Mtiling, C->Ntiling,
                   C->i, C->j,
                   C->mt, C->nt,
                   C->grid.rows,
                   C->dynamic_mode,
                   C->future_resolve_fct);
    Ccopy->allocate_mem = C->allocate_mem;

    irr_bs_tm_init(Arep, A->mtype,
                   A->super.nodes, A->super.myrank, A->lm, A->ln, A->mt, A->nt,
                   A->Mtiling, A->Ntiling,
                   0, 0, A->mt, A->nt, P,
                   IRR_TM_CREATE_ON_DEMAND, NULL);
    Arep->allocate_mem = 0;
    irr_tm_sparsity_copy(Arep, A);

    plan->A = Arep;
    th_arg = (gemm_irr_sparse_comm_thread_arg_t*)malloc(sizeof(gemm_irr_sparse_comm_thread_arg_t));
    th_arg->plan = plan;
    th_arg->Aref = A;
    th_arg->parsec = parsec;
    pthread_create(&th_arg->thread_id, NULL, gemm_irr_sparse_comm_thread, th_arg);
    
    parsec_zgemm_irr_sparse_genB_taskpool_t *handle =
        parsec_zgemm_irr_sparse_genB_new(ZGEMM_IRR_SPARSE_GENB, alpha, beta,
                                         Arep,
                                         B,
                                         C,
                                         Ccopy,
                                         &TrivDist,
                                         plan,
                                         nb_gpus, dev_index);
    adt = &handle->arenas_datatypes[PARSEC_zgemm_irr_sparse_genB_DEFAULT_ADT_IDX];
    handle->_g_RL = RL;
    handle->_g_CL = CL;
    handle->_g_GL = GL;
    handle->_g_comm_th_arg = th_arg;
    zgemm_handle = (parsec_taskpool_t*)handle;       

    parsec_datatype_t mtype;
    parsec_type_create_contiguous(1, parsec_datatype_double_complex_t, &mtype);

    parsec_arena_datatype_construct(adt, sizeof(parsec_complex64_t),
                                    PARSEC_ARENA_ALIGNMENT_SSE,
                                    mtype);

    return zgemm_handle;
}

/**
 *******************************************************************************
 *  zgemm_irr_sparse_Destruct - Free the data structure associated to an taskpool
 *  created with zgemm_irr_sparse_New().
 *
 *******************************************************************************
 *
 * @param[in,out] tp
 *          On entry, the taskpool to destroy.
 *          On exit, the taskpool cannot be used anymore.
 *
 ******************************************************************************/
void
zgemm_irr_sparse_Destruct( parsec_taskpool_t *tp )
{
    parsec_zgemm_irr_sparse_genB_taskpool_t *zgemm_taskpool = (parsec_zgemm_irr_sparse_genB_taskpool_t *)tp;
    if( zgemm_taskpool->_g_gemm_type == ZGEMM_IRR_SPARSE_GENB ) {
        pthread_join(zgemm_taskpool->_g_comm_th_arg->thread_id, NULL);
        zgemm_taskpool->_g_plan->A =  zgemm_taskpool->_g_comm_th_arg->Aref;
        irr_bs_tm_destroy((irr_bs_tm_t*)zgemm_taskpool->_g_descA);
        for(int k = 0; k < zgemm_taskpool->_g_descA->mt; k++) {
            if(NULL != zgemm_taskpool->_g_plan->Arepmem[k]) {
                free(zgemm_taskpool->_g_plan->Arepmem[k]);
                zgemm_taskpool->_g_plan->Arepmem[k] = NULL;
            }
        }
        if (zgemm_taskpool->arenas_datatypes[PARSEC_zgemm_irr_sparse_genB_DEFAULT_ADT_IDX].arena)
            parsec_matrix_del2arena( &zgemm_taskpool->arenas_datatypes[PARSEC_zgemm_irr_sparse_genB_DEFAULT_ADT_IDX] );
        free(zgemm_taskpool->_g_descC2->Mtiling);
        free(zgemm_taskpool->_g_descC2);
    }
    parsec_taskpool_free(tp);
}

