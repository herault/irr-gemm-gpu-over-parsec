/*
 * Copyright (c) 2016      The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * $COPYRIGHT
 *
 * @precisions normal z -> s d c
 *
 */

#include <string.h>

#include "parsec/data_dist/matrix/two_dim_rectangle_cyclic.h"
#include "irr_bs_tm.h"
#include "gemm_summit.h"
#include "zgemm_summit_NN_C.h"

#include "parsec/utils/mca_param.h"
static parsec_data_collection_t TrivDist;
static int TrivDistInitialized = 0;

static parsec_data_key_t TrivDist_data_key(parsec_data_collection_t *d, ...)
{
    va_list ap;
    int r;
    (void)d;
    va_start(ap, d);
    r = va_arg(ap, int);
    va_end(ap);
    return (parsec_data_key_t)r;
}

static uint32_t TrivDist_rank_of(parsec_data_collection_t *d, ...)
{
    va_list ap;
    int r;
    (void)d;
    va_start(ap, d);
    r = va_arg(ap, int);
    va_end(ap);
    return r;
}

static uint32_t TrivDist_rank_of_key(parsec_data_collection_t *d, parsec_data_key_t key)
{
    (void)d;
    (void)key;
    return (uint32_t)key;
}

static parsec_data_t *TrivDist_data_of(parsec_data_collection_t *d, ...)
{
    (void)d;
    assert(0);
    return NULL;
}

static parsec_data_t *TrivDist_data_of_key(parsec_data_collection_t *d, parsec_data_key_t key)
{
    (void)d;
    (void)key;
    assert(0);
    return NULL;
}

static int32_t TrivDist_vpid_of(parsec_data_collection_t *d, ...)
{
    (void)d;
    return 0;
}

static int32_t TrivDist_vpid_of_key(parsec_data_collection_t *d, parsec_data_key_t key)
{
    (void)d;
    (void)key;
    return 0;
}

/**
 *******************************************************************************
 *
 *  zgemm_summit_New - Generates the taskpool that performs one of the following
 *  matrix-matrix operations. WARNING: The computations are not done by this call.
 *
 *    \f[ C = \alpha [op( A )\times op( B )] + \beta C,
 *
 *  where op( X ) is one of
 *
 *    op( X ) = X  or op( X ) = X' or op( X ) = conjg( X' )
 *
 *  alpha is scalar, and A, B and C are matrices, with op( A )
 *  an m by k matrix, op( B ) a k by n matrix and C an m by n matrix.
 *
 *******************************************************************************
 *
 * @param[in] alpha
 *          alpha specifies the scalar alpha
 *
 * @param[in] A
 *          Descriptor of the distributed matrix A.
 *
 * @param[in] B
 *          Descriptor of the distributed matrix B.
 *
 * @param[in] beta
 *          beta specifies the scalar beta
 *
 * @param[out] C
 *          Descriptor of the distributed matrix C.
 *          On exit, the data described by C contain the matrix (
 *          alpha*op( A )*op( B ) )
 *
 *******************************************************************************
 *
 * @return
 *          \retval NULL if incorrect parameters are given.
 *          \retval The parsec taskpool describing the operation that can be
 *          enqueued in the runtime with parsec_enqueue(). It, then, needs to be
 *          destroy with zgemm_summit_Destruct();
 ******************************************************************************/
parsec_taskpool_t*
zgemm_summit_New( parsec_complex64_t alpha, const irr_bs_tm_t *A,
                  const irr_bs_tm_t *B,
                  parsec_complex64_t beta,
                  irr_bs_tm_t *C,
                  int b, int c, int d,
                  int p, int q,
                  int look_ahead)
{
    parsec_taskpool_t* zgemm_handle = NULL;
    parsec_arena_datatype_t* adt;
    int *dev_index, nb, dev;
    int Asize, Bsize, Csize;
    int u, v;

    if( TrivDistInitialized == 0 ) {
        TrivDistInitialized = 1;
        parsec_data_collection_init(&TrivDist, A->super.nodes, A->super.myrank);
        TrivDist.data_key = TrivDist_data_key;
        TrivDist.rank_of = TrivDist_rank_of;
        TrivDist.rank_of_key = TrivDist_rank_of_key;
        TrivDist.data_of = TrivDist_data_of;
        TrivDist.data_of_key = TrivDist_data_of_key;
        TrivDist.vpid_of = TrivDist_vpid_of;
        TrivDist.vpid_of_key = TrivDist_vpid_of_key;
        TrivDist.key_base = strdup("TrivDist");
        TrivDist.key_dim = strdup("");
    }
    
    Asize = A->mt * A->nt;
    Bsize = B->mt * B->nt;
    Csize = C->mt * C->nt;
                
    nb = 0;
    for(dev = 0; dev < (int)parsec_nb_devices; dev++) {
        parsec_device_module_t *device = parsec_mca_device_get(dev);
        if( PARSEC_DEV_CUDA == device->type ) {
            nb++;
        }
    }
    dev_index = (int*)malloc(nb * sizeof(int));
    nb = 0;
    for(dev = 0; dev < (int)parsec_nb_devices; dev++) {
        parsec_device_module_t *device = parsec_mca_device_get(dev);
        if( PARSEC_DEV_CUDA == device->type ) {
            dev_index[nb++] = device->device_index;
        }
    }
    
    // if( Bsize <= Asize || Bsize <= Csize)
    {
        parsec_zgemm_summit_NN_C_taskpool_t* handle;
        
        if( d > B->mt ) {
            fprintf(stderr, "Condition not met: d(%d) < K(%d)\n",
                    d, B->mt);
            return NULL;
        }
        if( b*p > A->mt ) {
            fprintf(stderr, "Condition not met: b(%d) * p(%d) < M(%d)\n",
                    b, p, A->mt);
            return NULL;
        }
        if( c*q > C->nt ) {
            fprintf(stderr, "Condition not met: c(%d) * q(%d) < N(%d)\n",
                    c, q, C->nt);
            return NULL;
        }

        handle = parsec_zgemm_summit_NN_C_new(ZGEMM_SUMMIT_NN_C, alpha, beta,
                                              A,
                                              B,
                                              C,
                                              &TrivDist,
                                              b, c, d, p, q, look_ahead,
                                              nb, dev_index);
        u = C->super.myrank / q;
        v = C->super.myrank % q;
        {
            int M = A->mt;
            int Mbound = M/(p*b);
            int Mlim = p*b*Mbound + u;
            handle->_g_xMax = Mbound + (Mlim < M) - 1;
#if 0
            printf("For rank (%d, %d): xMax = %d (M = %d, d=%d, MBound = %d, Mlim = %d)\n", u, v, handle->_g_xMax, M, b, Mbound, Mlim);
#endif
        }
        
        {
            int N = C->nt;
            int Nbound = N/(c*q);
            int Nlim = c*q*Nbound + v;
            handle->_g_yMax = Nbound + (Nlim < N) - 1;
#if 0
            printf("For rank (%d, %d): yMax = %d (N = %d, q=%d, c=%d, NBound = %d, Nlim = %d)\n", u, v, handle->_g_yMax, N, q, c, Nbound, Nlim);
#endif
        }
        
        {
            int K = B->mt;
            handle->_g_zMax = (K+d-1)/d - 1;
#if 0
            printf("For rank (%d, %d): zMax = %d (K = %d, p=%d, d=%d, KBound = %d, Klim = %d)\n", u, v, handle->_g_zMax, K, p, b, Kbound, Klim);
#endif
        }
        adt = &handle->arenas_datatypes[PARSEC_zgemm_summit_NN_C_DEFAULT_ADT_IDX];
        zgemm_handle = (parsec_taskpool_t*)handle;           
    }
    //}

    parsec_datatype_t mtype;
    parsec_type_create_contiguous(1, parsec_datatype_double_complex_t, &mtype);

    parsec_arena_datatype_construct(adt, sizeof(parsec_complex64_t),
                                    PARSEC_ARENA_ALIGNMENT_SSE,
                                    mtype);

    return zgemm_handle;
}

/**
 *******************************************************************************
 *  zgemm_summit_Destruct - Free the data structure associated to an taskpool
 *  created with zgemm_summit_New().
 *
 *******************************************************************************
 *
 * @param[in,out] tp
 *          On entry, the taskpool to destroy.
 *          On exit, the taskpool cannot be used anymore.
 *
 ******************************************************************************/
void
zgemm_summit_Destruct( parsec_taskpool_t *tp )
{
    parsec_zgemm_summit_NN_C_taskpool_t *zgemm_taskpool = (parsec_zgemm_summit_NN_C_taskpool_t *)tp;
    if( zgemm_taskpool->_g_gemm_type == ZGEMM_SUMMIT_NN_C ) {
        if (zgemm_taskpool->arenas_datatypes[PARSEC_zgemm_summit_NN_C_DEFAULT_ADT_IDX].arena)
            parsec_matrix_del2arena( &zgemm_taskpool->arenas_datatypes[PARSEC_zgemm_summit_NN_C_DEFAULT_ADT_IDX] );
    }
    parsec_taskpool_free(tp);
}

