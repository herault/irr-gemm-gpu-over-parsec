#ifndef GEMM_SUMMIT_H
#define GEMM_SUMMIT_H

#include "parsec.h"
#include <math.h>
#include <complex.h>

#define ZGEMM_SUMMIT_NN_C 1
#define DGEMM_SUMMIT_NN_C 2
#define SGEMM_SUMMIT_NN_C 3
#define CGEMM_SUMMIT_NN_C 4

#if defined(PARSEC_HAVE_CUDA)
#include <cublas.h>
typedef void (*cublas_sgemm_t) (char transa, char transb, int m, int n, int k, 
                               float alpha, const float *A, int lda, 
                               const float *B, int ldb, float beta, float *C, 
                               int ldc);
typedef void (*cublas_dgemm_t) (char transa, char transb, int m, int n, int k,
                               double alpha, const double *A, int lda, 
                               const double *B, int ldb, double beta, double *C, 
                               int ldc);              
typedef void (*cublas_cgemm_t) (char transa, char transb, int m, int n, int k, 
                               cuComplex alpha, const cuComplex *A, int lda,
                               const cuComplex *B, int ldb, cuComplex beta,
                               cuComplex *C, int ldc);
typedef void (*cublas_zgemm_t) (char transa, char transb, int m, int n,
                               int k, cuDoubleComplex alpha,
                               const cuDoubleComplex *A, int lda,
                               const cuDoubleComplex *B, int ldb,
                               cuDoubleComplex beta, cuDoubleComplex *C,
                               int ldc);                   
#endif  /* defined(PARSEC_HAVE_CUDA) */

parsec_taskpool_t*zgemm_summit_New( double complex alpha, const irr_bs_tm_t *A,
                                    const irr_bs_tm_t *B,
                                    double complex beta,
                                    irr_bs_tm_t *C,
                                    int b, int c, int d,
                                    int p, int q,
                                    int look_ahead);
parsec_taskpool_t*cgemm_summit_New( complex alpha, const irr_bs_tm_t *A,
                                    const irr_bs_tm_t *B,
                                    complex beta,
                                    irr_bs_tm_t *C,
                                    int b, int c, int d,
                                    int p, int q,
                                    int look_ahead);
parsec_taskpool_t*dgemm_summit_New( double alpha, const irr_bs_tm_t *A,
                                    const irr_bs_tm_t *B,
                                    double beta,
                                    irr_bs_tm_t *C,
                                    int b, int c, int d,
                                    int p, int q,
                                    int look_ahead);
parsec_taskpool_t*sgemm_summit_New( float alpha, const irr_bs_tm_t *A,
                                    const irr_bs_tm_t *B,
                                    float beta,
                                    irr_bs_tm_t *C,
                                    int b, int c, int d,
                                    int p, int q,
                                    int look_ahead);

void zgemm_summit_Destruct( parsec_taskpool_t *tp );
void cgemm_summit_Destruct( parsec_taskpool_t *tp );
void dgemm_summit_Destruct( parsec_taskpool_t *tp );
void sgemm_summit_Destruct( parsec_taskpool_t *tp );

#endif
